package com.example.bootsample.service;

import com.example.bootsample.entity.Todo;
import com.example.bootsample.persistence.TodoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TodoServiceImpl implements TodoService {

    private final TodoRepository repository;

    public TodoServiceImpl(TodoRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<Todo> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Todo findById(long id) {
        return repository.findById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void add(String newTodoText) {
        repository.insert(newTodoText);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void done(long id) {
        repository.changeDoneTrue(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void remove(long id) {
        repository.delete(id);
    }
}
